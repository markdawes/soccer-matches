package com.rave.soccermatches.viewmodel

import com.rave.soccermatches.model.local.SoccerMatch

/**
 * State that holds match data.
 *
 * @property matches
 * @property isLoading
 * @constructor Create empty Quote state
 */
data class MatchState(
    val matches: List<SoccerMatch> = emptyList(),
    val isLoading: Boolean = false
)
