package com.rave.soccermatches.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.soccermatches.model.SoccerRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Main view model.
 *
 * @property repo
 * @constructor Create empty Soccer view model
 */
@HiltViewModel
class SoccerViewModel @Inject constructor(private val repo: SoccerRepo) : ViewModel() {

    private val _state = MutableStateFlow(MatchState())
    val state get() = _state.asStateFlow()

    /**
     * Fetch match data to put in state.
     *
     */
    fun fetchMatches() {
        _state.update { it.copy(isLoading = true) }
        viewModelScope.launch {
            val matches = repo.getMatches()
            _state.update { it.copy(matches = matches, isLoading = false) }
        }
    }
}
