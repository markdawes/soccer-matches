package com.rave.soccermatches.view

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.rave.soccermatches.model.local.SoccerMatch
import com.rave.soccermatches.view.ui.theme.DarkBlue
import com.rave.soccermatches.view.ui.theme.SoccerMatchesTheme
import com.rave.soccermatches.view.ui.theme.Tan100
import com.rave.soccermatches.viewmodel.SoccerViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Entry point for application.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel by viewModels<SoccerViewModel>()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SoccerMatchesTheme {
                val state by viewModel.state.collectAsState()
                viewModel.fetchMatches()
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Tan100
                ) {
                    Row() {
                        Text(
                            text = "February 2019",
                            color = DarkBlue,
                            style = MaterialTheme.typography.displayMedium
                        )
                    }
                    LazyColumn(modifier = Modifier.padding(top = 88.dp)) {
                        items(state.matches) { match: SoccerMatch ->
                            MatchCard(match = match)
                        }
                    }
                }
            }
        }
    }
}

@Suppress("LongMethod")
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun MatchCard(match: SoccerMatch) {
    @Suppress("MagicNumber")
    val date = LocalDate.parse(match.date.take(10))
    val formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy")
    Card(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 4.dp, bottom = 2.dp),
        colors = CardDefaults.cardColors(
            containerColor = Color.White
        ),
        border = BorderStroke(
            width = 1.dp,
            color = Color.Gray
        ),
        shape = MaterialTheme.shapes.extraSmall
    ) {
        Row(modifier = Modifier.fillMaxSize()) {
            @Suppress("MagicNumber")
            Column(modifier = Modifier.weight(3f)) {
                Text(
                    text = "${match.compName}",
                    style = MaterialTheme.typography.headlineMedium
                )
                @Suppress("MagicNumber")
                Text(
                    text = "${match.venueName} | ${date.format(formatter)} at ${match.date.substring(11, 16)}",
                    style = MaterialTheme.typography.bodySmall,
                    color = Color.Gray
                )
                Text(
                    text = "${match.homeName}",
                    color = DarkBlue,
                    style = MaterialTheme.typography.headlineSmall
                )
                Text(
                    text = "${match.awayName}",
                    color = DarkBlue,
                    style = MaterialTheme.typography.headlineSmall
                )
            }
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                if (match.state == "postponed") {
                    Text("\n")
                    Text(
                        modifier = Modifier.background(Color.Red),
                        text = "postponed",
                        color = Color.White
                    )
                } else {
                    Text("\n")
                }
                @Suppress("MagicNumber")
                Text(
                    text = "${match.date.substring(8, 10)}",
                    color = DarkBlue,
                    style = MaterialTheme.typography.headlineLarge
                )
                @Suppress("MagicNumber")
                Text(
                    text = "${date.dayOfWeek.toString().take(3)}",
                    color = DarkBlue,
                    style = MaterialTheme.typography.headlineMedium
                )
            }
        }
    }
}
