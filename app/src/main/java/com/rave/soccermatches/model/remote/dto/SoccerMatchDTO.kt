package com.rave.soccermatches.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class SoccerMatchDTO(
    val awayTeam: AwayTeamDTO,
    val competitionStage: CompetitionStageDTO,
    val date: String,
    val homeTeam: HomeTeamDTO,
    val id: Int,
    val state: String?,
    val type: String,
    val venue: VenueDTO
)
