package com.rave.soccermatches.model.remote

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.create

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private val mediaType = "application/json".toMediaType()

    @Provides
    fun providesJson(): Json = Json {
        ignoreUnknownKeys = true
        explicitNulls = false
        coerceInputValues = true
    }

    @Provides
    fun providesRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://storage.googleapis.com/cdn-og-test-api/test-task/")
            .addConverterFactory(providesJson().asConverterFactory(mediaType))
            .build()
    }

    @Provides
    fun providesAPIService(retrofit: Retrofit): APIService = retrofit.create()
}
