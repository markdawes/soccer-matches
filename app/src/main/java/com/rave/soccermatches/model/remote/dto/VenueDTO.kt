package com.rave.soccermatches.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class VenueDTO(
    val id: Int,
    val name: String
)
