package com.rave.soccermatches.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class AwayTeamDTO(
    val abbr: String,
    val alias: String,
    val id: Int,
    val name: String,
    val shortName: String
)
