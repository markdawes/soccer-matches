package com.rave.soccermatches.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class CompetitionDTO(
    val id: Int,
    val name: String
)
