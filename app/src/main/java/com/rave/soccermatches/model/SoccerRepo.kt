package com.rave.soccermatches.model

import com.rave.soccermatches.model.local.SoccerMatch
import com.rave.soccermatches.model.remote.APIService
import javax.inject.Inject

/**
 * Soccer repo.
 *
 * @property service
 * @constructor Create empty Soccer repo
 */
class SoccerRepo @Inject constructor(private val service: APIService) {

    /**
     * Return match data as SoccerMatch objects.
     *
     * @return
     */
    suspend fun getMatches(): List<SoccerMatch> {
        val matchDTOs = service.getMatchData()
        return matchDTOs.map {
            SoccerMatch(
                compName = it.competitionStage.competition.name,
                venueName = it.venue.name,
                homeName = it.homeTeam.name,
                awayName = it.awayTeam.name,
                id = it.id,
                date = it.date,
                state = it.state,
                type = it.type
            )
        }
    }
}
