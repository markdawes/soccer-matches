package com.rave.soccermatches.model.remote

import com.rave.soccermatches.model.remote.dto.SoccerMatchDTO
import retrofit2.http.GET

/**
 * Service class that creates the endpoint needed.
 *
 * @constructor Create empty A p i service
 */
interface APIService {

    @GET("fixtures.json")
    suspend fun getMatchData(): List<SoccerMatchDTO>
}
