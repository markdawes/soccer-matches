package com.rave.soccermatches.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class CompetitionStageDTO(
    val competition: CompetitionDTO,
    val leg: String?,
    val stage: String?
)
