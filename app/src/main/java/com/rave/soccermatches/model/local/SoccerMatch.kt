package com.rave.soccermatches.model.local

/**
 * Soccer match.
 *
 * @property compName
 * @property venueName
 * @property homeName
 * @property awayName
 * @property date
 * @property id
 * @property state
 * @property type
 * @constructor Create empty Soccer match
 */
data class SoccerMatch(
    val compName: String,
    val venueName: String,
    val homeName: String,
    val awayName: String,
    val date: String,
    val id: Int,
    val state: String?,
    val type: String
)
