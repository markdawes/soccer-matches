package com.rave.soccermatches.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
class MatchesResponse : ArrayList<SoccerMatchDTO>()
