package com.rave.soccermatches

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Custom application class.
 *
 * @constructor Create empty Soccer application
 */
@HiltAndroidApp
class SoccerApplication : Application()
